<?php
  // 16.1.2014

  $DB_BASIC_VERSION = 1;
  /*
    $DB_BASIC_VERSION historia:
    verzia 1: zakladna verzia
  //*/
   
  setlocale(LC_CTYPE, 'sk_SK.UTF-8');
  setlocale(LC_TIME, 'sk_SK');
  date_default_timezone_set("Europe/Brussels");                                                        


  require("config/config.inc.php");

  //if (file_exists("config/specials.inc.php")) include "config/specials.inc.php";

  $TABLE_EVIDENCE = "objednavky";
  $TABLE_PERSONS = "identifikacia";
//  $TABLE_MEMBERS = "members";
//  $TABLE_LOGIN_ATTEMPTS = "login_attempts";
  
	$collate = "utf8";
 
   /** kodovanie retazcov do UTF-8 */                                    
  function encodeToUtf8($string) {
     return mb_convert_encoding($string, "UTF-8", "UTF-8");
  }

  if (!function_exists('iconv') && function_exists('libiconv')) {
    function iconv($input_encoding, $output_encoding, $string) {
        return libiconv($input_encoding, $output_encoding, $string);
    }
  }
  /** odstranenie diakritiky */                                    
  function removeDia($c) {
    $tx = iconv('UTF-8', 'CP1250//TRANSLIT//IGNORE', $c);
    if ($tx=="") {
      $c = str_replace("ľ","l",$c);
      $c = str_replace("š","s",$c);
      $c = str_replace("č","c",$c);
      $c = str_replace("ť","t",$c);
      $c = str_replace("ž","z",$c);
      $c = str_replace("ý","y",$c);
      $c = str_replace("á","a",$c);
      $c = str_replace("í","i",$c);
      $c = str_replace("é","e",$c);
      $c = str_replace("ú","u",$c);
      $c = str_replace("ä","a",$c);
      $c = str_replace("ô","o",$c);
      $c = str_replace("ů","u",$c);
      $c = str_replace("ř","r",$c);
      $c = str_replace("ĺ","l",$c);
      $c = str_replace("ď","d",$c);
      $c = str_replace("ň","n",$c);
      $c = str_replace("ó","o",$c);
      $c = str_replace("ŕ","r",$c);
      
      $c = str_replace("Ľ","L",$c);
      $c = str_replace("Š","S",$c);
      $c = str_replace("Č","C",$c);
      $c = str_replace("Ť","T",$c);
      $c = str_replace("Ž","Z",$c);
      $c = str_replace("Ý","Y",$c);
      $c = str_replace("Á","A",$c);
      $c = str_replace("Í","I",$c);
      $c = str_replace("É","E",$c);
      $c = str_replace("Ú","U",$c);
      $c = str_replace("Ů","U",$c);
      $c = str_replace("Ř","R",$c);
      $c = str_replace("Ĺ","L",$c);
      $c = str_replace("Ď","D",$c);
      $c = str_replace("Ň","N",$c);
      $c = str_replace("Ó","O",$c);
      $c = str_replace("Ř","R",$c);
      return $c;
    }
    return $tx;
  }

  // Connecting DB
  try {
    //# MS SQL Server and Sybase with PDO_DBLIB
    //$DBH = new PDO("mssql:host=$host;dbname=$dbname, $user, $pass");
    //$DBH = new PDO("sybase:host=$host;dbname=$dbname, $user, $pass");
    
    switch ($__TYP__DATABAZY__) {
      case "local": 
        $LOCAL_HOST1 = "localhost"; 
        $LOCAL_USER1 = "root";
        $LOCAL_PASS1 = "111111";
        //# MySQL with PDO_MYSQL
        $DBH = new PDO("mysql:host=$LOCAL_HOST1;dbname=$DB_NAME;charset=$collate", $LOCAL_USER1, $LOCAL_PASS1);
        break;
      case "server_tablet": 
        $LOCAL_HOST1 = "localhost"; 
        $LOCAL_USER1 = "root";
        $LOCAL_PASS1 = "111111";
        //# MySQL with PDO_MYSQL
        $DBH = new PDO("mysql:host=$LOCAL_HOST1;dbname=$DB_NAME;charset=$collate", $LOCAL_USER1, $LOCAL_PASS1);
        break;
      case "sqlite_tablet": 
        //# SQLite Database
        $DBH = new PDO("sqlite:". $data_path ."/". $DB_NAME . ".db");
        break;
    }

    if ($__TYP__DATABAZY__ != "sqlite_tablet") {
        $mysqli = new mysqli($LOCAL_HOST1, $LOCAL_USER1, $LOCAL_PASS1, $DB_NAME);  // login script
        $mysqli->set_charset($collate);
    }
  }
  catch(PDOException $e) {
      echo $e->getMessage();
  }
			
                              	
  try {
    $DBH->exec("SET NAMES $collate");   
    $DBH->exec("SET character_set_connection $collate");                               	
    $DBH->exec("SET character_set_client $collate");                               	
    $DBH->exec("SET character_set_results $collate");                 
                                          	
    $DBH->exec("USE $DB_NAME");                               	
  }
  catch(PDOException $e) {
    die('Error: '.$e->getMessage());
  }      
      
  // *** dni v tyzdni
	$dni[0] = "nedeľa";
	$dni[1] = "pondelok";
	$dni[2] = "utorok";
	$dni[3] = "streda";
	$dni[4] = "štvrtok";
	$dni[5] = "piatok";
	$dni[6] = "sobota";
	$dni[7] = $dni[0];
  
  // *** mesiace v roku
  $month_name = array("", "Január", "Február", "Marec", "Apríl", "Máj", "Jún", 
               "Júl", "August", "September", "Október", "November", "December");
    
  // php time is system time (timezone), MySQL timezone is unknown - tablet problem     
  $now = time();  
  $date_time_now = "'".date("Y-m-d H:i:s")."'";

  $full_url_path = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"]);

?>
