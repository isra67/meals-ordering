<?php
  // Objednavanie stravy - login
  // 16.1.2014

  require("dbconnectlib.php");
?>

<!DOCTYPE html>
<html lang="sk">
<head>

<title>Objednávanie stravy</title>

<META http-equiv="Content-Type" content="text/html; charset=utf-8">
<META http-equiv="Cache-Control" content="no-cache">
<META http-equiv="Pragma" content="no-cache"> 

  <link rel="stylesheet" type="text/css" href="styles/styles.css">
  
  <style type="text/css">    
  
    a.menua { text-decoration:none;display:inline-block;width:140px; }
    .menua { 
      font-weight:bold;font-size:24px;color:#800000;
      margin:0px 5px 10px 5px;padding:12px 35px;border:2px solid #B22222;border-radius:4px; 
    }

  </style>
  
  <script src="js/jquery-1.10.2.min.js" type="text/javascript"></script>
  <script src="js/jquery-ui.min.js" type="text/javascript"></script>

  <!--
  <l i n k rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/start/jquery-ui.css">
  -->
      
  <script type="text/javascript" charset="utf-8">

  
  $(document).ready(function() {

  });
    
      
  </script>  
  
</head>

<body>
  
  <p>&nbsp;</p>
  
  <h2> Stravovanie </h2>

 	<a class="menua yesshadowbox" href="index.html"> Nastavenia </a>
  <a class="menua yesshadowbox" href="objednavanie.html"> Objednávka </a>    
  <a class="menua yesshadowbox" href="vydaj.html"> Výdaj </a>    

  <p>&nbsp;</p>
  
  <p>
  <label>Systém objednávania stravy od firmy Inoteska s.r.o. Liptovský Hrádok</label>
  </p>
  
  <p>&nbsp;</p>
  
  <hr>
  <h6><?php echo $COPY_NAME; ?></h6>

</body>
</html>
