<?php
  // Objednavanie stravy - login
  // 21.1.2014

  require("dbconnectlib.php");


  if (!isset($_REQUEST['task'])) {
    array_push($res_arr,array('ERROR' => 'Task ERROR!'));
    echo json_encode($res_arr);  // vysledok ako JSON 
    die();
  }

  $func = $_REQUEST['task'];
    
  $results = "";
  $res_arr = array();


//echo "*** $func<br>";


  /*************************************************************************/
  if ($func == "changeDbMan") {
    // zmena objednavky (storno alebo odber)
    if (!isset($_REQUEST['id'],$_REQUEST['typ'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
              
    $id = $_REQUEST['id'];
    
    $query = ($_REQUEST['typ'] == 1)?
      "UPDATE $TABLE_EVIDENCE SET odobrate=NOW() WHERE id='$id'" :
      "DELETE FROM $TABLE_EVIDENCE WHERE id='$id'";
  
    try {
      $count = $DBH->exec($query);
      if ($count === false) {
        echo "\nPDO::errorInfo():\n";
        print_r($DBH->errorInfo());
        array_push($res_arr,array('ERROR' => "SQL query failed!"));
      } else {
        array_push($res_arr,array('Result' => 'OK' ));
      }
    }
    catch(PDOException $e) {
      array_push($res_arr,array('ERROR' => $e->getMessage() ));
    }            
  
  } else   
  /*************************************************************************/
  if ($func == "deleteDbMan") {
    // vymazanie registrovaneho
    if (!isset($_REQUEST['id'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }

    $id = $_REQUEST['id'];
    
	  $query = "DELETE FROM $TABLE_PERSONS WHERE id='$id'";
    try {
      $count = $DBH->exec($query);
      if ($count === false) {
        echo "\nPDO::errorInfo():\n";
        print_r($DBH->errorInfo());
        array_push($res_arr,array('ERROR' => "SQL query failed!"));
      } else {
        array_push($res_arr,array('Result' => 'Vymazane: $count' ));
      }
    }
    catch(PDOException $e) {
      array_push($res_arr,array('ERROR' => $e->getMessage() ));
    }            
  } else 
  /*************************************************************************/
  if ($func == "setpeople") {
    // zapis registrovanych ludi
    if (!isset($_REQUEST['id'],$_REQUEST['n'],$_REQUEST['s'],$_REQUEST['p'],$_REQUEST['c1'],$_REQUEST['c2'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }

    $id = $_REQUEST['id'];
    $meno = $_REQUEST['n'];
    $priezv = $_REQUEST['s'];
    $pozn = $_REQUEST['p'];
    $kod1 = $_REQUEST['c1'];
    $kod2 = $_REQUEST['c2'];

    $tkod1 = ($kod1=="")? "?":$kod1;
    $tkod2 = ($kod2=="")? "?":$kod2;
    
    // test jedinecnosti parametrov:    
    if ($id == 0) {
      $query = 
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE meno LIKE '$meno' AND priezvisko LIKE '$priezv' AND poznamka LIKE '$pozn') UNION ALL ".
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE kod LIKE '%$tkod1%') UNION ALL ".        
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE kod LIKE '%$tkod2%')";        
    } else {
      $query = 
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE id<'0') UNION ALL ".
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE id<>'$id' AND kod LIKE '%$tkod1%') UNION ALL ".        
        "(SELECT count(id) FROM $TABLE_PERSONS WHERE id<>'$id' AND kod LIKE '%$tkod2%')";        
    }

//array_push($res_arr,array('query' => "$query"));
        
    $result = $DBH->query($query);  
    if ($result === false) {
      array_push($res_arr,array('ERROR' => "SQL query failed!"));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    } else {                
  		//$result->setFetchMode(PDO::FETCH_ASSOC);  

      $i = 0;      
      $temp = 0; 
      $errstr = "";    
     	while (($row = $result->fetch()) != NULL) {
        if ($row[0] > 0) {
          switch ($i) {
            case 0: $errstr .= "<br> - meno a priezvisko"; break; 
            case 1: $errstr .= "<br> - číselný kód"; break; 
            case 2: $errstr .= "<br> - RFID kód"; break; 
          }          
        }
        $i++;        
        $temp += $row[0];        
      }
      $result->closeCursor();
      
      if ($temp > 0) {
        array_push($res_arr,array('ERROR' => "Konflikt zadaných údajov $errstr !"));
        echo json_encode($res_arr);  // vysledok ako JSON 
        die();
      }
    }     
     
//array_push($res_arr,array('temp' => "$temp"));
//echo json_encode($res_arr);  // vysledok ako JSON 
        
    if ($id == 0) { 
      // najdenie maximalneho pouzivaneho 'evid'
      $query = "SELECT MAX(evid) AS m FROM $TABLE_PERSONS"; 
  
      $maxevid=1;
      
      $result = $DBH->query($query);  
      if ($result === false) {
        array_push($res_arr,array('ERROR' => "SQL query failed!"));
        echo json_encode($res_arr);  // vysledok ako JSON 
        die();
      } else {                
    		$result->setFetchMode(PDO::FETCH_ASSOC);  
               
       	while (($row = $result->fetch()) != NULL) {
          $maxevid = $row['m'] + 1;        
        }
        $result->closeCursor(); 
      }
      
      // vlozenie novej osoby
      $query = "INSERT INTO $TABLE_PERSONS (meno,priezvisko,poznamka,kod,kateg,evid) VALUES " .
        "('$meno','$priezv','$pozn','$kod1,$kod2','0','$maxevid')";
    } else {
      // uprava osoby
      $query = "UPDATE $TABLE_PERSONS SET meno='$meno',priezvisko='$priezv',poznamka='$pozn',kod='$kod1,$kod2' " .
        "WHERE id=$id";
    }

//array_push($res_arr,array('query' => "$query"));
//echo json_encode($res_arr);  // vysledok ako JSON 
        
   	try {
      $STH = $DBH->prepare($query);
      if (!$STH) {
        print_r($DBH->errorInfo());
        array_push($res_arr,array('ERROR' => "SQL query failed!" ));
      } else {
        $stmt = $STH->execute();
        if (!$stmt) {
          print_r($STH->errorInfo());
          array_push($res_arr,array('ERROR' => "SQL query failed!" ));
        } else {    
          array_push($res_arr,array('Result' => 'OK' ));
        }
      }
    }
		catch(PDOException $e) {  
      array_push($res_arr,array('ERROR' => $e->getMessage() ));
		}    
//echo json_encode($res_arr);  // vysledok ako JSON 
    
  } else 
  /*************************************************************************/
  if ($func == "getpeople") {
    // zobrazenie registrovanych ludi
    
    $query = "SELECT * FROM $TABLE_PERSONS ORDER BY poznamka,priezvisko,meno"; 

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
             
     	while (($row = $result->fetch()) != NULL) {
        $arr = array('id' => $row['id'],'meno' => $row['meno'],'priezvisko' => $row['priezvisko'], 
          'poznamka' => $row['poznamka'],'evid' => $row['evid'],'kod' => $row['kod'],'kateg' => $row['kateg']);
        array_push($res_arr,$arr);
      }
      $result->closeCursor(); 
    } else {
      array_push($res_arr,array('ERROR' => "SQL query failed!" ));
    }
    
  } else 
  /*************************************************************************/
  if ($func == "getorders") {
    // zobrazenie objednavok
    if (!isset($_REQUEST['t'],$_REQUEST['d'],$_REQUEST['full'])) die("Parameter ERROR");
    
    $t = $_REQUEST['t'];
    $dat = $_REQUEST['d'];
    $full = $_REQUEST['full'];

    $where = ($t == 1) ? "DAY($TABLE_EVIDENCE.den)=DAY('$dat')" : "MONTH($TABLE_EVIDENCE.den)=MONTH('$dat')";
    $where .= " AND YEAR($TABLE_EVIDENCE.den)=YEAR('$dat')";
    if ($full == 0)
      $where .= " AND DAY($TABLE_EVIDENCE.den)>=DAY('$dat')";
         
    $query = "SELECT $TABLE_EVIDENCE.id,$TABLE_EVIDENCE.vyber,$TABLE_EVIDENCE.odobrate,$TABLE_EVIDENCE.osoba,".
             "DATE_FORMAT($TABLE_EVIDENCE.den,'%d.%m.') AS d,DATE_FORMAT($TABLE_EVIDENCE.stamp,'%d.%m. %H:%i') AS s,".
             "DATE_FORMAT($TABLE_EVIDENCE.odobrate,'%H:%i') AS o,".
             "$TABLE_PERSONS.id AS ido,$TABLE_PERSONS.evid AS evid,".
             "$TABLE_PERSONS.meno AS me,$TABLE_PERSONS.priezvisko AS pr,$TABLE_PERSONS.poznamka AS po ". 
             "FROM $TABLE_EVIDENCE ".
             "INNER JOIN $TABLE_PERSONS ".
             "ON $TABLE_EVIDENCE.osoba=$TABLE_PERSONS.evid ".
             "WHERE $where ORDER BY $TABLE_PERSONS.poznamka,$TABLE_EVIDENCE.den,$TABLE_EVIDENCE.stamp"; 
    if ($full == 0)
      $query .= " LIMIT 5";

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
             
     	while (($row = $result->fetch()) != NULL) {
        $arr = array('id' => $row['id'], 'den' => $row['d'], 'osoba' => $row['osoba'], 'objednavka' => $row['vyber'], 
                    'odobrate' => $row['odobrate'] != '0000-00-00 00:00:00', 'caso' => $row['o'], 'casobj' => $row['s'],
                    'meno' => $row['pr'] . " " . $row['me'],'poznamka' => $row['po'],
                    'ido' => $row['ido'],'evid' => $row['evid']);
        //$arr = array('id' => $row['id'], 'objednavka' => $row['d'] . ' - ' . $row['vyber']);
        array_push($res_arr,$arr);
      }
      $result->closeCursor(); 
    } else {
      array_push($res_arr,array('ERROR' => "SQL query failed!" ));
    }
/*        
    if (count($res_arr) == 0) {
      array_push($res_arr,array('ERROR' => 'no data! - ' . $query));
    }  //*/
  } else 
  /*************************************************************************/
  if ($func == "save") {
    // zapis objednavky
    if (!isset($_REQUEST['i'],$_REQUEST['d'],$_REQUEST['s'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
    
    $id = $_REQUEST['i'];
    $dat = $_REQUEST['d'];
    $sel = $_REQUEST['s'];
    
    $id_rec = 0; // existujuci zaznam
    
    // test existencie zaznamu pre den
    $query = "SELECT id FROM $TABLE_EVIDENCE WHERE osoba='$id' AND den='$dat'"; 

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
             
     	while (($row = $result->fetch()) != NULL) {
        $id_rec = $row['id'];        
      }
      $result->closeCursor(); 
    } else {
      array_push($res_arr,array('ERROR' => "SQL query failed!" ));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
        
    if ($id_rec == 0) { 
      // vlozenie objednavky ID
      $query = "INSERT INTO $TABLE_EVIDENCE (den,osoba,vyber,stamp,odobrate) VALUES " .
        "('$dat','$id','$sel',$date_time_now,'0000-00-00 00:00:00')";
    } else {
      // uprava objednavky ID
      $query = "UPDATE $TABLE_EVIDENCE SET vyber='$sel',stamp=$date_time_now,odobrate='0000-00-00 00:00:00' " .
        "WHERE id=$id_rec";
    }
   	try {
      $STH = $DBH->prepare($query);
      if (!$STH) {
        print_r($DBH->errorInfo());
        array_push($res_arr,array('ERROR' => "SQL query failed!" ));
      } else {
        $stmt = $STH->execute();
        if (!$stmt) {
          print_r($STH->errorInfo());
          array_push($res_arr,array('ERROR' => "SQL query failed!" ));
        } else {    
          array_push($res_arr,array('Result' => 'OK' ));
        }
      }
    }
		catch(PDOException $e) {  
      array_push($res_arr,array('ERROR' => $e->getMessage() ));
		}    
  } else 
  /*************************************************************************/
  if ($func == "check") {
    // jedlo osoby na den
    if (!isset($_REQUEST['i'],$_REQUEST['d'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
    
    $id = $_REQUEST['i'];
    $dat = $_REQUEST['d'];
    
    $query = "SELECT id,DATE_FORMAT(den,'%d.%m.') AS d,DATE_FORMAT(stamp,'%d.%m.') AS s,vyber,odobrate,DATE_FORMAT(odobrate,'%H:%i') AS o ".
             "FROM $TABLE_EVIDENCE WHERE osoba='$id' AND den='$dat' ORDER BY stamp LIMIT 10"; 

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
             
     	while (($row = $result->fetch()) != NULL) {
        $arr = array('id' => $row['id'], 'den' => $row['d'], 'objednavka' => $row['vyber'], 
                    'odobrate' => $row['odobrate'] != '0000-00-00 00:00:00', 'caso' => $row['o'], 'casobj' => $row['s']);
        //$arr = array('id' => $row['id'], 'objednavka' => $row['d'] . ' - ' . $row['vyber']);
        array_push($res_arr,$arr);
      }
      $result->closeCursor(); 
    } else {
      array_push($res_arr,array('ERROR' => "SQL query failed!" ));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }

    if (isset($_REQUEST['vydaj'])) {
      if (count($res_arr) == 0) {
        $arr = array('id' => $id, 'objednavka' => '','odobrate' => false, 'caso' => '00:00');
        array_push($res_arr,$arr);

        // zaznam odberu bez objednavky 
        $query = "INSERT INTO $TABLE_EVIDENCE (den,osoba,vyber,stamp,odobrate) VALUES " .
          "('$dat','$id','0','0000-00-00 00:00:00',$date_time_now)";
      } else {
        // zaznam odberu objednavky 
        $query = "UPDATE $TABLE_EVIDENCE SET odobrate=$date_time_now " .
          "WHERE osoba='$id' AND den='$dat' AND odobrate='0000-00-00 00:00:00'";
      }
     	try {
        $STH = $DBH->prepare($query);
        if (!$STH) {
          print_r($DBH->errorInfo());
          array_push($res_arr,array('ERROR' => "SQL query failed!" ));
        } else {
          $stmt = $STH->execute();
          if (!$stmt) {
            print_r($STH->errorInfo());
            array_push($res_arr,array('ERROR' => "SQL query failed!" ));
          }
        }
      }
  		catch(PDOException $e) {  
        array_push($res_arr,array('ERROR' => $e->getMessage() ));
  		}    
    }

  } else 
  /*************************************************************************/
  if ($func == "find") {
    // identifikacia osoby
    if (!isset($_REQUEST['code'])) {
      array_push($res_arr,array('ERROR' => 'Parameter ERROR!'));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
    
    $code = $_REQUEST['code'];
    $id = 0;
  
    $query = "SELECT * FROM $TABLE_PERSONS WHERE kod LIKE '%$code%' LIMIT 1"; 

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
                     
     	while (($row = $result->fetch()) != NULL) {
        $id = $row['evid'];	    
        $arr = array('id' => $row['id'], 'meno' => $row['meno'], 'priezvisko' => $row['priezvisko'],'kateg' => $row['kateg'], 
          'evid' => $row['evid'], 'poznamka' => $row['poznamka'],'kod' => $row['kod'], 'objednavky' => '');
        array_push($res_arr,$arr);
      }
      $result->closeCursor(); 
    } else {
      array_push($res_arr,array('ERROR' => 'no data!' ));
      echo json_encode($res_arr);  // vysledok ako JSON 
      die();
    }
    
    // zoznam objednanych jedal
    $query = "SELECT DATE_FORMAT(den,'%d.%m.') AS d,vyber FROM $TABLE_EVIDENCE ".
             "WHERE osoba='$id' AND den=NOW() ORDER BY den LIMIT 5"; 

    $result = $DBH->query($query);  
    if ($result !== false) {                 
  		$result->setFetchMode(PDO::FETCH_ASSOC);  
             
     	while (($row = $result->fetch()) != NULL) {
        $res_arr[0]['objednavky'] .= $row['d'] . ' - ' . $row['vyber'] . '; ';
      }
      $result->closeCursor(); 
    }

  } else {
    /*************************************************************************/
//    $results = "Chybný príkaz!";
    array_push($res_arr,array('ERROR' => 'bad command!'));
    echo json_encode($res_arr);  // vysledok ako JSON
    die(); 
  }
           
  if (count($res_arr) == 0) {
    array_push($res_arr,array('ERROR' => 'no data!'));
  }
          
  if ($results == "")                  // vysledok ako plain text              
    $results = json_encode($res_arr);  // vysledok ako JSON 
  
  echo $results;
  	  
  // close the connection
  $DBH = null;

?>
